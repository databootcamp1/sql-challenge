# sql-challenge
Creator: Melissa Acevedo

## Introduction

Design the tables to hold the data from the CSV files, import the CSV files into a SQL database, and then answer questions about the data. That is, data modeling, data engineering, and data analysis.

## Table of Contents

- [Data Modeling](#data-modeling)
- [Data Engineering](#data-engineering)
- [Data Analysis](#data-analysis)

## Data Modeling

Sketch an Entity Relationship Diagram of the tables.

- departments.csv
- dept_emp.csv
- dept_manager.csv
- employees.csv
- salaries.csv
- titles.csv

## Data Engineering

Table schema for each of the six CSV files. Included are data types, primary keys, foreign keys, and other constrants. 

## Data Analysis

Provide SQL queries and results for the following analyses:

1. List the employee number, last name, first name, sex, and salary of each employee.
2. List the first name, last name, and hire date for employees hired in 1986.
3. List the manager of each department with department number, department name, employee number, last name, and first name.
4. List the department number for each employee with employee number, last name, first name, and department name.
5. List first name, last name, and sex of employees with first name Hercules and last name starting with B.
6. List each employee in the Sales department with employee number, last name, and first name.
7. List each employee in the Sales and Development departments with employee number, last name, first name, and department name.
8. List the frequency counts, in descending order, of all employee last names.
