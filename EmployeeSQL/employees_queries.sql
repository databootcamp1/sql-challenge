SELECT e.emp_no, e.last_name, e.first_name, e.sex, s.salary
FROM "EMPLOYEES" e
JOIN "SALARIES" s ON e.emp_no = s.emp_no
ORDER BY e.emp_no ASC;


SELECT first_name, last_name, hire_date
FROM "EMPLOYEES"
WHERE EXTRACT(YEAR FROM hire_date) = 1986;


SELECT
    dm.dept_no,
    d.dept_name,
    dm.emp_no AS manager_emp_no,
    e.last_name AS manager_last_name,
    e.first_name AS manager_first_name
FROM "DEPARTMENTS" d
JOIN "DEPT_MANAGER" dm ON d.dept_no = dm.dept_no
JOIN "EMPLOYEES" e ON dm.emp_no = e.emp_no;


SELECT
    e.emp_no,
    e.last_name,
    e.first_name,
    de.dept_no,
    d.dept_name
FROM "EMPLOYEES" e
JOIN "DEPT_EMP" de ON e.emp_no = de.emp_no
JOIN "DEPARTMENTS" d ON de.dept_no = d.dept_no;


SELECT first_name, last_name, sex
FROM "EMPLOYEES"
WHERE first_name = 'Hercules' AND last_name LIKE 'B%';


SELECT e.emp_no, e.last_name, e.first_name
FROM "EMPLOYEES" e
JOIN "DEPT_EMP" de ON e.emp_no = de.emp_no
JOIN "DEPARTMENTS" d ON de.dept_no = d.dept_no
WHERE d.dept_name = 'Sales';


SELECT
    e.emp_no,
    e.last_name,
    e.first_name,
    d.dept_name
FROM "EMPLOYEES" e
JOIN "DEPT_EMP" de ON e.emp_no = de.emp_no
JOIN "DEPARTMENTS" d ON de.dept_no = d.dept_no
WHERE d.dept_name IN ('Sales', 'Development');


SELECT last_name,COUNT(*) AS frequency
FROM "EMPLOYEES"
GROUP BY last_name
ORDER BY frequency DESC;
